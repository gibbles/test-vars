# Playing for Understanding

## gitstuff

The gitstuff job is me working out how to make gitlab publish tags back into the repo. I found the answer in the following posts.

- [thoean](https://thoean.com/push-git-tag-from-gitlab-runner/)
- [stackoverflow](https://stackoverflow.com/questions/43787094/using-gitlab-deploy-keys-with-write-access)
- [devops.stackexchange](https://devops.stackexchange.com/questions/3794/how-to-tag-source-code-using-gitlabci)

I had to create a key pair, saving the public key in a deploy key with write access turned on, and saving the private key in a CI/CD variable.

## test

The test job was the original use case for this repository, and was me verifying how the overrides of maps and lists worked out with global sections as well as extending hidden jobs.
